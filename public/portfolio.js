const model = {
    // finnhubToken: "cc3nfp2ad3i9vsk3u7gg",
    finnhubToken: "cl1hefhr01qkvip7qe40cl1hefhr01qkvip7qe4g",
    isSearchingCryptos: false,
    userData: JSON.parse(localStorage.getItem("userData")) || {
        assets: {
            GOOG: {symbol: "GOOG", shares: 1, type: "stock", price: 100},
            AAPL: {symbol: "AAPL", shares: 0, type: "stock", price: 100},
            AMZN: {symbol: "AMZN", shares: 0, type: "stock", price: 100},
            BTCUSDT: {symbol: "BTCUSDT", shares: 0, type: "crypto", price: 100},
        },
        info: {
            balance: 10000,
        },
    },
    timerInterval: null,
    timeLeft: 15,
    setTimeLeft(time) {
        model.timeLeft = time;
        controller.updateTimer();
    },
    focusedAsset: null,
    setFocusedAsset(asset) {
        this.focusedAsset = asset;
        view.updateFocus(asset);
    },
};

const view = {
    updateFocus(asset) {
        view.removeChildren(focusedDescription);
        view.removeChildren(focusedCTAs);

        const stockHeadlines = document.createElement("p");
        stockHeadlines.classList.add(
            "flex-center",
            "space-between",
            "ampler-padding",
            "half-width"
        );
        const stockSymbol = document.createElement("span");
        stockSymbol.innerText = asset.symbol;
        const stockPrice = document.createElement("span");
        stockPrice.innerText = "Loading";
        view.updateElementAssetPrice(stockPrice, asset.price);
        stockHeadlines.append(stockSymbol, stockPrice);
        focusedDescription.appendChild(stockHeadlines);

        const usersAsset = model.userData.assets[asset.symbol];
        const usersShares = usersAsset?.shares;

        const buyButton = view.makeMaterialButton("Buy");
        buyButton.addEventListener("click", function () {
            controller.buyStock(asset.symbol);
        });
        focusedCTAs.appendChild(buyButton);

        if (usersShares) {
            const userStake = document.createElement("p");
            userStake.classList.add(
                "flex-center",
                "space-between",
                "ampler-padding",
                "half-width"
            );
            const numberOfShares = document.createElement("span");
            numberOfShares.innerText = `${usersShares} shares`;
            const valuation = document.createElement("span");
            valuation.innerText = "Loading...";
            view.updateElementAssetValuation(
                valuation,
                asset.price,
                usersShares,
                false
            );
            userStake.append(numberOfShares, valuation);
            focusedDescription.appendChild(userStake);
            const sellButton = view.makeMaterialButton("Sell");
            sellButton.addEventListener("click", () => {
                controller.sellStock(asset.symbol);
            });
            focusedCTAs.appendChild(sellButton);
        }

        if (usersAsset) {
            //this is equivalent to the fact that it is being watched
            const ignoreButton = view.makeMaterialButton("Ignore");
            ignoreButton.addEventListener("click", function () {
                controller.ignoreStock(asset.symbol);
            });
            focusedCTAs.appendChild(ignoreButton);
        } else {
            const watchButton = view.makeMaterialButton("Watch");
            watchButton.addEventListener("click", function () {
                controller.watchStock(asset.symbol);
            });
            focusedCTAs.appendChild(watchButton);
        }

        const stockDescription = document.createElement("p");
        stockDescription.innerText = `This lorem concerning ${asset.symbol} ipsum dolor sit amet consectetur adipisicing elit. Tempore, at ullam repellendusexpedita aperiam optio, rem quos voluptate ea facere velit cumcommodi placeat nesciunt deserunt quidem. Aspernatur,repellendus nobis?`;
        focusedDescription.appendChild(stockDescription);

        tickerSearchInput.value = asset.symbol;
    },
    alerUser(message) {
        alertDialog.open();
        alertContent.innerText = message;
    },
    dismissAlert() {
        alertDialog.close();
    },
    refreshPage(stockInFocus) {
        console.log(stockInFocus);
        controller.focusTicker(
            controller.fetchAssetPrice({
                symbol: stockInFocus,
                type: model.isSearchingCryptos ? "crypto" : "stock",
            })
        );
        controller.setStorage();
        view.refreshAssetList();
        view.intializeMaterialList();
    },
    updateElementAssetPrice(element, price) {
        element.innerText = view.displayDollars(price);
    },
    updateElementAssetValuation(element, price, numShares, isPrefixed = true) {
        const prefix = isPrefixed ? `${numShares} shares:` : "";
        const displayText = prefix + view.displayDollars(price * numShares);
        element.innerText = displayText;
    },
    refreshAssetList() {
        view.removeChildren(stockList);
        for (asset in model.userData.assets) {
            view.createAssetListItem(model.userData.assets[asset]);
        }
    },
    displayDollars(number) {
        return number; //`$${number.toFixed(2)}`;
    },
    removeChildren(domNode) {
        while (domNode.firstChild) {
            domNode.removeChild(domNode.firstChild);
        }
    },
    makeMaterialButton(buttonAction) {
        const newButton = document.createElement("button");
        newButton.classList.add(
            "mdc-button",
            "mdc-card__action",
            "mdc-card__action--button"
        );

        const rippleDiv = document.createElement("div");
        rippleDiv.classList.add("mdc-button__ripple");
        newButton.appendChild(rippleDiv);

        const labelSpan = document.createElement("span");
        labelSpan.classList.add("mdc-button__label");
        labelSpan.innerText = buttonAction;
        newButton.appendChild(labelSpan);

        return newButton;
    },
    displaySearchError(errorMessage) {
        searchHelperText.innerText = errorMessage;
        searchHelperText.classList.add(
            "mdc-text-field-helper-text--validation-msg"
        );
        searchHelperText.classList.remove("mdc-text-field-helper-text");
    },
    hideSearchError() {
        searchHelperText.innerText = "";
        searchHelperText.classList.remove(
            "mdc-text-field-helper-text--validation-msg"
        );
        searchHelperText.classList.add("mdc-text-field-helper-text");
    },
    createAssetListItem(asset) {
        const newListItem = document.createElement("li");
        newListItem.classList.add("mdc-list-item", "space-between");
        newListItem.setAttribute("role", "option");
        newListItem.setAttribute("tabindex", "0");
        newListItem.setAttribute("data-ticker", asset.symbol);

        const rippleSpan = document.createElement("span");
        rippleSpan.classList.add("mdc-list-item__ripple");
        newListItem.appendChild(rippleSpan);

        const textSpan = document.createElement("span");
        textSpan.classList.add("mdc-list-item__text");
        textSpan.innerText = asset.symbol;
        newListItem.appendChild(textSpan);
        const usersShares = asset.shares;
        const sharesSpan = document.createElement("span");
        if (usersShares) {
            sharesSpan.innerText = "Loading ... ";
            view.updateElementAssetValuation(
                sharesSpan,
                asset.symbol,
                usersShares
            );
        } else {
            sharesSpan.innerText = `just watching`;
        }
        newListItem.appendChild(sharesSpan);
        const priceSpan = document.createElement("span");
        priceSpan.innerText = " Loading ...";
        newListItem.appendChild(priceSpan);
        view.updateElementAssetPrice(priceSpan, asset.symbol);
        stockList.appendChild(newListItem);
    },
    initializePage() {
        view.updateFocus(Object.values(model.userData.assets)[0]);
        // view.updateFocus(model.userData.assets.GOOG);
        view.refreshAssetList();
        view.initializeHelperText();
    },
    intializeMaterialList() {
        const MDCList = mdc.list.MDCList;
        const MDCRipple = mdc.ripple.MDCRipple;
        const list = new MDCList(document.querySelector(".mdc-list"));
        const listItemRipples = list.listElements.map(
            (listItemEl) => new MDCRipple(listItemEl)
        );
    },
    initializeHelperText() {
        const MDCTextFieldHelperText = mdc.textField.MDCTextFieldHelperText;
        const helperText = new MDCTextFieldHelperText(
            document.querySelector(".mdc-text-field-helper-text")
        );
    },
};

const controller = {
    updateTimer() {
        timerSpan.innerText = model.timeLeft;
    },

    startTimer() {
        model.setTimeLeft(5);
        model.timerInterval = setInterval(() => {
            if (model.timeLeft == 0) {
                return this.timeoutTrade(model.timerInterval);
            }
            model.setTimeLeft(model.timeLeft - 1);
        }, 1000);
    },

    timeoutTrade(interval) {
        clearInterval(interval);
        tradeDialog.close();
        view.alerUser("Please Confirm Trade");
    },

    buyStock(stock) {
        console.log(stock);
        // tradeDialog.open();
        // this.startTimer();

        if (model.userData.assets[stock].shares) {
            model.userData.assets[stock].shares += 1;
        } else {
            model.userData.assets[stock].shares = 1;
        }
        view.refreshPage(stock);
    },
    sellStock(stock) {
        model.userData.assets[stock].shares -= 1;
        view.refreshPage(stock);
    },
    watchStock(stock) {
        model.userData.assets[stock].shares = 0;
        view.refreshPage(stock);
    },
    ignoreStock(stock) {
        if (model.userData.assets[stock].shares) {
            alert(" it is prudent to sell before you ignore! ");
        } else {
            delete model.userData.assets[stock];
        }
        view.refreshPage(stock);
    },
    async fetchStockPrice(stockSymbol) {
        try {
            const res = await fetch(
                `https://finnhub.io/api/v1/quote?symbol=${stockSymbol}&token=${model.finnhubToken}`
            );
            const data = await res.json();
            if (data.c == 0)
                throw `Oh no! Probably ${stockSymbol} is not a stock`;

            console.log(data);
            view.hideSearchError();
            return {price: data.c, symbol: stockSymbol, type: "stock"};
        } catch (thrownError) {
            view.displaySearchError(thrownError);
        }
    },
    async fetchCryptoPrice(cryptoSymbol) {
        const binanceBaseUrl = "https://api.binance.us";
        try {
            const res = await fetch(
                binanceBaseUrl + `/api/v3/ticker/price?symbol=${cryptoSymbol}`
            );
            const data = await res.json();
            data.price = parseFloat(parseFloat(data.price).toFixed(2));
            data["type"] = "crypto";
            console.log(data);
            view.hideSearchError();
            return data;
        } catch (_) {
            console.log("Failed to fetch " + cryptoSymbol);
            view.displaySearchError(`Sure ${cryptoSymbol} is a crypto?`);
            return {symbol: "None", price: 0};
        }
    },
    fetchAssetPrice(asset) {
        return asset.type === "crypto"
            ? controller.fetchCryptoPrice(asset.symbol)
            : controller.fetchStockPrice(asset.symbol);
    },
    toggleSearchMode() {
        focusHeadline.innerText = model.isSearchingCryptos
            ? "Searching Stocks"
            : "Searching Cryptos";
        toggleButton.childNodes[3].innerText = model.isSearchingCryptos
            ? "Search Cryptos"
            : "Search Stocks";
        model.isSearchingCryptos = !model.isSearchingCryptos;
    },
    async searchTicker() {
        const searchedTicker = tickerSearchInput.value;
        if (controller.validateTickerInput(searchedTicker)) {
            const data = await controller.fetchAssetPrice({
                symbol: searchedTicker,
                type: model.isSearchingCryptos ? "crypto" : "stock",
            });
            model.setFocusedAsset(data);
        } else {
            view.displaySearchError("you are searching for nothing");
        }
    },
    validateTickerInput(tickerSymbol) {
        return tickerSymbol !== "";
    },
    async focusTicker(symbol) {
        console.log(symbol);
        const data = await controller.fetchAssetPrice(symbol);
        model.setFocusedAsset(data);
    },
    focusListItem(event) {
        let nodeOfInterest = event.target;
        if (nodeOfInterest.matches("ul")) return;
        while (!nodeOfInterest.matches("li")) {
            nodeOfInterest = nodeOfInterest.parentNode;
        }
        const dataAttribute = nodeOfInterest.dataset["ticker"];
        controller.focusTicker(model.userData.assets[dataAttribute]);
    },
    setStorage() {
        localStorage.setItem("userData", JSON.stringify(model.userData));
    },
};

// MDC
const tradeDialog = new mdc.dialog.MDCDialog(
    document.querySelector(".mdc-dialog")
);

const alertDialog = new mdc.dialog.MDCDialog(
    document.querySelector("#alert-dialog")
);

const timerSpan = document.querySelector("#trade-timer");

const alertContent = document.querySelector("#alert-content");
const alertButton = document.querySelector("#alert-button");
alertButton.addEventListener("click", view.dismissAlert);
// end of MDC

const focusHeadline = document.querySelector("#focus-headline");
const focusedDescription = document.querySelector("#focused-description");
const focusedCTAs = document.querySelector("#focused-ctas");
const toggleButton = document.querySelector("#toggle-focus");
toggleButton.addEventListener("click", controller.toggleSearchMode);

const tickerSearchIcon = document.querySelector("#ticker-search");
const tickerSearchInput = document.querySelector("#ticker-input");
const searchHelperText = document.querySelector("#helper-text");
tickerSearchIcon.addEventListener("click", controller.searchTicker);
tickerSearchInput.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        tickerSearchIcon.click();
    }
});
const stockList = document.querySelector("ul#stock-list");
stockList.addEventListener("click", controller.focusListItem);

view.initializePage();
// leave the socket stuff for next time
let socket;

function openBinanceWebsocket() {
    socket = new WebSocket(
        "wss://stream.binance.com:9443/ws/btcusdt@miniTicker"
    );

    socket.onopen = function (event) {
        console.log("Websocket connetion open", event);
    };

    socket.onmessage = function (event) {
        console.log(event);
        let data = JSON.parse(event.data);
        console.log(data);
        console.log(
            "current bitcoin price",
            view.displayDollars(parseFloat(data.c))
        );
    };
}

function subscribeBinanceSymbol(symbol) {
    socket.send(
        JSON.stringify({
            method: "SUBSCRIBE",
            params: [`${symbol}@miniTicker`],
            id: 1,
        })
    );
}
