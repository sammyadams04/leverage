const token = "bu91lg748v6t2erimf3g";

function fetchStockPrice(stockSymbol, callback) {
    fetch(
        `https://finnhub.io/api/v1/quote?symbol=${stockSymbol}&token=${token}`
    )
        .then((response) => response.json())
        .then((data) => {
            callback(data);
        });
}

// console.log(fetchStockPrice("AAPL", (data) => console.log(data)));

// const binanceBaseUrl = "https://api.binance.us";
// function fetchCryptoPrice(cryptoSymbol, callback) {
//     fetch(binanceBaseUrl + `/api/v3/ticker/price?symbol=${cryptoSymbol}`)
//         .then((response) => response.json())
//         .then((data) => callback(data));
// }

// fetchCryptoPrice("BTCUSDT", (data) => console.log(data));

// WebSockets

const wsBaseURL = "wss://stream.binance.us:9443";

function openBinanceWebSocket() {
    const socket = new WebSocket(wsBaseURL + "/ws/btcusdt@miniTicker");

    socket.onopen = function (event) {
        console.log("Connection Open", event);
    };

    socket.onmessage = function (event) {
        // console.log(event);
        let data = JSON.parse(event.data);
        // console.log(data);
        console.log("current btc price:", parseFloat(data.c));
    };
}

//openBinanceWebSocket();

// MVC = Model View Controller

// model = data

// View => user facing interface

// controller => communication between view and model. Controls the things that happens

const binanceBaseUrl = "https://api.binance.us";
// function fetchCryptoPrice(cryptoSymbol, callback) {
//     fetch(binanceBaseUrl + `/api/v3/ticker/price?symbol=${cryptoSymbol}`)
//         .then((response) => response.json())
//         .then((data) => callback(data));
// }

async function fetchCryptoPrice(cryptoSymbol) {
    try {
        const res = await fetch(
            binanceBaseUrl + `/api/v3/ticker/price?symbol=${cryptoSymbol}`
        );
        const data = await res.json();
        data["type"] = "crypto";
        return data;
    } catch (_) {
        console.log("Failed to fetch " + cryptoSymbol);
        return {symbol: "None", price: 0};
    }
}

async function wrapper() {
    const cryptoData = await fetchCryptoPrice("BTCUSDT1");
    console.log(cryptoData);
}

wrapper();
