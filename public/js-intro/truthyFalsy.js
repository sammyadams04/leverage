if (true) {
    console.log("we should be surprised not to see this");
}
if (false) {
    console.log("if you see me we have a problem");
}
if (3 - 2) {
    console.log("numbers are truthy");
}
if (!(2 - 2)) {
    console.log("except for zero");
}
if ("a string value") {
    console.log("strings are truthy");
}
if (!"") {
    console.log("except the empty string");
}

console.log("all the falsy values are");
console.log("false, null, undefined, 0, NaN, '' ");

console.log("short circuiting");
console.log(
    "|| returns the first truthy value:",
    "" || 0 || false || null || "the truthy one" || "another truthy"
);
console.log(
    "&& returns the first falsy value:",
    2 && true && "foobarbaz" && false && ""
);
