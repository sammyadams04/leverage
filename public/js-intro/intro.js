let x = 10;

console.log(x);

x = 15;
console.log(x);

const pi = 3.14;
console.log(pi);
// pi = 3; can't change the constant

const u = 6.18;
console.log(u);

// conditional
let attendance = 70;
// >= : '> ='
if (attendance >= 80) {
    console.log("Attendance requirement met");
} else if (attendance <= 80 && attendance >= 70) {
    console.log("Attendance requirement almost met");
} else if (attendance != 100) {
} else {
    console.log("Attendance requirement not met");
}
// == : = =
// === : = = =
attendance = 70; // int, numeric

attendance = "100"; // string
if (attendance == 100) {
    console.log("we got a 100");
}
if (attendance === "100") {
    console.log("we got a type string of 100");
}
if (attendance === 100) {
    // not running at all
    console.log("well this is type int of value 100");
}

// !== : ! = =
// != : ! =

if (attendance !== "90") {
    console.log("well it is not 90");
}

if (attendance === "90" || attendance === "100") {
    console.log("well it is either 90 or 100");
}

let z = 17;
// this is a lie, sort of
if (17 < z < 20) {
    console.log(z);
}
console.log(17 < z);
z = -5;
if (-10 < z < 2) {
    console.log(z);
}
console.log(17 < z);
console.log(false - true); // 0 - 1
// scope
let outside = "outside"; // global scope
console.log(outside);

{
    // console.log(outside);
    let outside = "in here";
    console.log(outside);

    let inside = "inside";
    console.log(inside);

    var global_inside = "inside"; // global scope
}
console.log(outside);
// console.log(inside); // inside is local scope
console.log(global_inside);

// String
console.log(
    `Momma said "Don't foreget about backtic... "` +
        `... because their power is over ${9 * 1000}`
);

// arrays
const fruits = ["apple", "banana", "cherry", "strawberry", "orange", 5, 99.9];

// len(fruits) = fruit.length
// i++ : i += 1
for (let i = 0; i < fruits.length; i++) {
    console.log(i, fruits[i]);
}

// es-6 to the rescue
for (const fruit of fruits) {
    console.log(fruit);
}

for (ch of "fruits") {
    console.log(ch);
}

fruits[0] = "mango";

console.log(fruits);

const elements = ["earth", "fire", "wind", "water"];
console.log(elements.length);
elements.push("heart");
console.log(elements.length);
for (const element of elements) {
    console.log(element);
}
console.log("By your powers combined... I AM CAPTIAN PLANET!");

// functions
function square(n) {
    return n * n;
}

console.log(square(3));

function divMod(x, y) {
    return [Math.floor(x / y), x % y];
}

const cookies = 17;
const kids = 5;
const [cookiesPerKid, extraCookies] = divMod(cookies, kids);
console.log(cookiesPerKid, extraCookies);

const squareArrow = (n) => n * n;
console.log(squareArrow(3));

const quoteRemArrow = (x, y) => {
    return [Math.floor(x / y), x % y];
};
console.log(quoteRemArrow(cookies, kids));

const celebrate = () => console.log("Woohoo!");
celebrate();

// objects

const dirtyChai = {
    expresso: 2,
    isChai: true,
    milk: "Whole",
    isIced: false,
};
dirtyChai.sprinkles = "cinnamon"; // this is more js

key = "isChai";
dirtyChai[key] = false;
//dirtyChai["isChai"] = false;
console.log(dirtyChai);

function FancyDrink(expresso, milkStyle, isIced = false, isChai = false) {
    this.expresso = expresso;
    this.milk = milkStyle;
    this.isChai = isChai;
    this.drink = function () {
        console.log("that was delicious!");
    };
}

// class FancyDrink {
//     constructor(expresso, milkStyle, isIced = false, isChai = false) {
//         this.expresso = expresso;
//         this.milk = milkStyle;
//         this.isChai = isChai;
//         // this.drink = function () {
//         //     console.log("that was delicious!");
//         // };
//     }
//     drink() {
//         console.log("drink!");
//     }
// }

const latte = new FancyDrink(2, "whole");
const greatPumpkin = new FancyDrink(0, "Whole");
console.log(latte);
console.log(greatPumpkin);
