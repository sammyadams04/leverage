function howfarAboveZero(number) {
    debugger; // blocking
    if (number == 0) return 0;
    else return 1 + howfarAboveZero(number - 1);
}
// howfarAboveZero(10);

function demostrateMessageQueue() {
    console.log("first");
    setTimeout(() => console.log("eighth"), 1000);
    console.log("second");
    setTimeout(() => console.log("sixth"));
    console.log("third");
    setTimeout(() => console.log("seventh"), 0);
    console.log("fourth");
    return "fifth";
}
// demostrateMessageQueue();

function showSyncException() {
    alert("is this blocking?"); // synchronous | blocking
    console.log("totally");

    const isBlocking = confirm("this is blocking too?"); //synchronous | blocking
    isBlocking ? console.log("right on") : console.log("we are learning");

    const userInput = prompt("taking user input this way blocks execution");
    console.log(userInput);

    console.log("perhaps");
}

showSyncException();
